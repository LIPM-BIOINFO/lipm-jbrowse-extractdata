
# Extract Features JBrowse Plugin

The source code is now available @ [https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/lipm-jbrowse-extractdata](https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/lipm-jbrowse-extractdata/)

# Contact
Sebastien.Carrere@inrae.fr
